using System.Text.RegularExpressions;

namespace Tela.utils;

public class Validator
{
    // Regular expression patterns for email and name validation
    private static readonly Regex EmailRegex = new(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");
    private static readonly Regex NameRegex = new(@"^[a-zA-Z0-9_-]{3,16}$");

    // Method to validate email using regular expression
    public static bool ValidateEmail(string email)
    {
        return EmailRegex.IsMatch(email);
    }

    // Method to validate name using regular expression
    public static bool ValidateName(string name)
    {
        return NameRegex.IsMatch(name);
    }
}