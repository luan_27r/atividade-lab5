using System;

namespace Tela.helpers.errors;

public class InvalidParameterError : Exception
{
    public InvalidParameterError(string parameterName)
        : base($"Invalid value for parameter '{parameterName}'.")
    {
        ParameterName = parameterName;
    }

    public string ParameterName { get; }
}

public class InvalidNameError : Exception
{
    public InvalidNameError(string message) : base(message)
    {
    }
}