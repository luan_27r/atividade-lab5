using System;
using System.Collections.Generic;

namespace Tela.helpers.errors;

public class ErrorStack
{
    private static readonly Stack<ErrorInfo> stack = new();

    public static void PushError(string errorMessage)
    {
        stack.Push(new ErrorInfo(errorMessage, DateTime.Now));
    }

    public static string PopError()
    {
        return stack.Count == 0
            ? "No errors in the stack."
            : $"[{stack.Pop().Timestamp}] Invalid param error: {stack.Peek().ErrorMessage}";
    }

    public static string PeekErrors()
    {
        if (stack.Count == 0) return "No errors in the stack.";

        var errors = "Errors in the stack:\n";
        foreach (var errorInfo in stack) errors += $"[{errorInfo.Timestamp}] {errorInfo.ErrorMessage}\n";
        return errors;
    }

    private class ErrorInfo
    {
        public ErrorInfo(string errorMessage, DateTime timestamp)
        {
            ErrorMessage = errorMessage;
            Timestamp = timestamp;
        }

        public string ErrorMessage { get; }
        public DateTime Timestamp { get; }
    }
}