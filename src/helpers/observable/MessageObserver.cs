using System.Diagnostics;

namespace Tela.helpers.observable;

public class MessageObserver : IObservable
{
    // Method to update the observer with a message
    public void Update(string message)
    {
        // Create a new process to execute the notification command
        using var process = new Process
        {
            StartInfo =
            {
                FileName = "notify-send", // Command for displaying notifications on Linux
                Arguments = $"\"Notification\" \"{message}\"", // Message argument for the command
                UseShellExecute = false, // Do not use the shell to execute the process
                CreateNoWindow = true // Do not create a window for the process
            }
        };

        // Start the process
        process.Start();
        // Wait for the process to exit
        process.WaitForExit();
    }
}