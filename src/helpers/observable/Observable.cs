namespace Tela.helpers.observable;

public interface IObservable
{
    void Update(string message);
}