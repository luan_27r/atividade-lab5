using System;
using System.Collections.Generic;

namespace Tela.helpers.observable;

public class Subject
{
    // List to store observers
    private readonly List<IObservable> observers = new();

    // Method to attach an observer
    public void Attach(IObservable observer)
    {
        observers.Add(observer);
    }

    // Method to detach an observer
    public void Detach(IObservable observer)
    {
        observers.Remove(observer);
    }

    // Method to notify all observers
    public void Notify(string message)
    {
        foreach (var observer in observers)
            try
            {
                // Call the Update method of each observer
                observer.Update(message);
            }
            catch (Exception err)
            {
                // Handle any errors that occur during notification
                Console.WriteLine($"Error notifying observer: {err.Message}");
            }
    }
}