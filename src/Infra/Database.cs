using System.Collections.ObjectModel;
using Tela.helpers.errors;
using Tela.helpers.observable;
using Tela.Models;
using Tela.utils;

namespace Tela.Infra;

public class Database
{
    // Singleton instance of the database
    private static Database? instance;

    // Collection to store users
    private readonly ObservableCollection<User> users = new();

    // Ensuring singleton pattern with lazy initialization
    public static Database CreateInstance()
    {
        return instance ??= new Database();
    }

    // Method to save a new user to the database
    public void SaveUser(User user)
    {
        // Validate user input
        ValidateUser(user);

        // Add user to the collection
        users.Add(user);

        // Notify observers about the new user
        NotifyObservers($"User {user.Email} has been added.");
    }

    // Method to validate user input
    private void ValidateUser(User user)
    {
        // Validate email
        if (!Validator.ValidateEmail(user.Email))
            throw new InvalidParameterError("Invalid email");

        // Validate name
        if (!Validator.ValidateName(user.Name))
            throw new InvalidParameterError("Invalid name");

        // Validate surname
        if (!Validator.ValidateName(user.Surname))
            throw new InvalidParameterError("Invalid surname");
    }

    // Method to notify observers about an event
    private void NotifyObservers(string message)
    {
        var messageObserver = new MessageObserver();
        var subject = new Subject();
        subject.Attach(messageObserver);
        subject.Notify(message);
    }

    // Method to get the collection of users
    public ObservableCollection<User> GetUsers()
    {
        return users;
    }
}