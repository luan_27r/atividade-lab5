﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Tela.Infra;
using Tela.Models;

namespace Tela.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    // Collection of users

    // Collection of users before any updates

    // Public property to access the users collection
    public static ObservableCollection<User> Users { get; } = new();

    // Public property to access the users collection before any updates
    public static ObservableCollection<User> UsersBeforeUpdate { get; } = new();

    // Method to add a new user
    public static void AddNewUser(User user)
    {
        // Save user to the database
        Database.CreateInstance().SaveUser(user);
        // Add user to the current users collection
        Users.Add(user);
        // Add user to the users collection before any updates
        UsersBeforeUpdate.Add(user);
    }

    // Method to filter users based on email
    public static void FilterUser(string? textToFilter)
    {
        // Trim whitespace from filter text or use an empty string if null
        var textWithoutSpace = textToFilter?.Trim() ?? string.Empty;

        // Check if filter text is empty or null
        if (IsNullOrEmptyText(textWithoutSpace)) return;

        // Filter users based on email containing filter text
        var filteredUsers = UsersBeforeUpdate.Where(obj => obj.Email.Contains(textWithoutSpace)).ToList();

        // Clear current users collection and add filtered users
        Users.Clear();
        ImplementFilteredUsers(filteredUsers);
    }

    // Method to check if filter text is null or empty
    private static bool IsNullOrEmptyText(string text)
    {
        // If filter text is empty or null, restore all users
        if (string.IsNullOrEmpty(text))
        {
            Users.Clear();
            foreach (var user in UsersBeforeUpdate) Users.Add(user);

            return true;
        }

        return false;
    }

    // Method to add filtered users to the current users collection
    private static void ImplementFilteredUsers(IEnumerable<User> filteredUsers)
    {
        foreach (var user in filteredUsers) Users.Add(user);
    }
}