using System.Text;

namespace Tela.Models;

public class User
{
    // Constructor to initialize user's properties
    public User(string name, string email, string surname)
    {
        Name = name;
        Email = email;
        Surname = surname;
    }

    // Properties for user's email, surname, and name
    public string Email { get; set; }
    public string Surname { get; set; }
    public string Name { get; set; }

    // Override ToString method to provide custom string representation of the user object
    public override string ToString()
    {
        // Create a StringBuilder to efficiently construct the string
        var text = new StringBuilder();
        // Append email, surname, and name to the string
        text.AppendLine($"Email: {Email}");
        text.AppendLine($"Surname: {Surname}");
        text.AppendLine($"Name: {Name}");
        // Return the string representation
        return text.ToString();
    }
}