using System;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Tela.helpers.errors;
using Tela.Models;
using Tela.ViewModels;

namespace Tela.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    // Handle user registration
    public void Handle(object sender, RoutedEventArgs e)
    {
        try
        {
            // Get user input
            var nameText = name.Text;
            var emailText = email.Text;
            var surnameText = surname.Text;

            // Check if all fields are filled
            var existsAllFields = string.IsNullOrEmpty(nameText) || string.IsNullOrEmpty(emailText) ||
                                  string.IsNullOrEmpty(surnameText);
            if (existsAllFields) return;

            // Create new user and add to the list
            User user = new(nameText!, emailText!, surnameText!);
            MainWindowViewModel.AddNewUser(user);

            // Show success message
            successLabel.Text = $"Parabéns! Você se cadastrou com sucesso. Bem-vindo à nossa comunidade, {user.Name}!";
            successLabel.IsVisible = true;
            errorLabel.IsVisible = false;

            // Clear input fields
            Clear();
        }
        catch (Exception ex)
        {
            // Handle error
            HandleError(ErrorStack.PopError(), ex);
        }
    }

    // Handle error display
    private void HandleError(string errorMessage, Exception exception)
    {
        errorLabel.Text = errorMessage;
        errorLabel.IsVisible = true;
        successLabel.IsVisible = false;
    }

    // Filter users based on email
    private void FilterTextUpdate(object sender, KeyEventArgs e)
    {
        MainWindowViewModel.FilterUser(filterByEmail.Text);
    }

    // Clear input fields
    private void Clear()
    {
        name.Text = "";
        email.Text = "";
        surname.Text = "";
    }
}