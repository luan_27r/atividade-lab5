using System;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Tela.ViewModels;

namespace Tela;

public class ViewLocator : IDataTemplate
{
    // Build method to create view based on view model
    public Control? Build(object? data)
    {
        // Check if data is a ViewModelBase
        if (data is not ViewModelBase viewModel)
            return null;

        // Get the type of the corresponding view
        var viewType = GetViewType(viewModel.GetType());
        if (viewType is null)
            // If view type not found, return a TextBlock with error message
            return new TextBlock
            {
                Text =
                    $"Not Found: {viewModel.GetType().FullName!.Replace("ViewModel", "View", StringComparison.Ordinal)}"
            };

        // Create an instance of the view and set its DataContext to the view model
        var view = (Control)Activator.CreateInstance(viewType)!;
        view.DataContext = viewModel;
        return view;
    }

    // Match method to determine if data is a ViewModelBase
    public bool Match(object? data)
    {
        return data is ViewModelBase;
    }

    // Helper method to get the type of the corresponding view
    private static Type? GetViewType(Type viewModelType)
    {
        var viewModelTypeName = viewModelType.FullName!;
        var viewTypeName = viewModelTypeName.Replace("ViewModel", "View", StringComparison.Ordinal);
        return Type.GetType(viewTypeName);
    }
}